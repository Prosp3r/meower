package event

import (
	"gitlab.com/prosp3r/meower/schema"
)

//EventStore defines interface for any event store type...all has to implement these methods
type EventStore interface {
	Close()
	PublishMeowCreated(meow schema.Meow) error
	SubscribeMeowCreated() (<-chan MeowCreatedMessage, error)
	OnMeowCreated(f func(MeowCreatedMessage)) error
}

//Declare an Eventstore type
var impl EventStore

//SetEventStore declares an event store variable
func SetEventStore(es EventStore) {
	impl = es
}

//Close implements the Close() method required by the EventStore interface
func Close() {
	impl.Close()
}

//PublishMeowCreated implements the named method to meet the EventStore type requirement
func PublishMeowCreated(meow schema.Meow) error {
	return impl.PublishMeowCreated(meow)
}

//SubscribeMeowCreated implements the named method to meet the EventStore type requirement
func SubscribeMeowCreated() (<-chan MeowCreatedMessage, error) {
	return impl.SubscribeMeowCreated()
}

//OnMeowCreated implements the named method to meet the EventStore type requirement
func OnMeowCreated(f func(MeowCreatedMessage)) error {
	return impl.OnMeowCreated(f)
}
