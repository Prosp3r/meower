package event

import (
	"time"
)

//Message interface for message types for Nats message alerts
type Message interface {
	Key() string
}

//MeowCreatedMessage type structure for other example instances: UserCreated or PlayRequested
type MeowCreatedMessage struct {
	ID        string
	Body      string
	CreatedAt time.Time
}

//Key function Implements the type Message interface.
func (m *MeowCreatedMessage) Key() string {
	return "meow.created"
}
