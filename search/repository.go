package search

import (
	"context"

	"gitlab.com/prosp3r/meower/schema"
)

//Repository defines the repository interface for this(search) service/package
type Repository interface {
	Close()
	InsertMeow(ctx context.Context, meow schema.Meow)
	SearchMeows(ctx context.Context, query string, skip uint64, take uint64) ([]schema.Meow, error)
}

var impl Repository

func SetRepository(repository Repository) {
	impl = repository
}

//Close an open repository connection
func Close() {
	impl.Close()
}

//InsertMeow into the database
func InsertMeow(ctx context.Context, meow schema.Meow) error {
	return impl.InsertMeow(ctx, meow)
}

//SearchMeows already in databse
func SearchMeows(ctx context.Context, query string, skip uint64, take uint64) ([]schema.Meow, error) {
	return impl.SearchMeows(ctx, query, skip, take)
}
