package db

import (
	"context"

	"gitlab.com/prosp3r/meower/schema"
)

//Repository interface defines all the functions a repository type must impplement
type Repository interface {
	Close()
	InsertMeow(ctx context.Context, meow schema.Meow) error
	ListMeows(ctx context.Context, skip uint64, take uint64) ([]schema.Meow, error)
}

var impl Repository

//SetRepository dynamically sets a variable to the type Repository
func SetRepository(repository Repository) {
	impl = repository
}

//Close implements the Close function for the type Repository. Closes the implementations
func Close() {
	impl.Close()
}

//InsertMeow implements its named function as required by the type
func InsertMeow(ctx context.Context, meow schema.Meow) error {
	return impl.InsertMeow(ctx, meow)
}

//ListMeows implements the named function for the Repository type
func ListMeows(ctx context.Context, skip uint64, take uint64) ([]schema.Meow, error) {
	return impl.ListMeows(ctx, skip, take)
}
