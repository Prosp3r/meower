package schema

import (
	"time"
)

//Meow type defines the structure for data base table for storing Meows with and ID and when they're created
type Meow struct {
	ID        string    `json:"id"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"created_at"`
}

//User type defines the schema for users data table
type User struct {
	ID           uint64    `json:"id"`
	Email        string    `json:"email"`
	Firstname    string    `json:"firstname"`
	Lastname     string    `json:"lastname"`
	UserLocation string    `json:"user_location"`
	Password     string    `json:"password"`
	CreatedAt    time.Time `json:"created_at"`
}
