package util

import (
	"encoding/json"
	"net/http"
)

//ResponseOk standard json confirmation respose(header and body) ok to any third party that tried to communicate with the system
func ResponseOk(w http.ResponseWriter, body interface{}) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(body)
}

//ResponseError returns standard json error response(header and body) alerting the thirdparty there was an error in communication
func ResponseError(w http.ResponseWriter, code int, message string) {
	w.WriteHeader(code)
	w.Header().Set("Content-Type", "application/json")

	body := map[string]string{
		"error": message,
	}
	json.NewEncoder(w).Encode(body)
}
